// To ensure we use strict mode 
'use strict'
// Include the File System module
const fs = require('fs');

// Counter class with different kinds of functions
// Public methods are execute() -> to execute counting and check input file
// And logResults -> to log the result
export class Counter{
    // Our inputfile
    inputFile: string;
    // Store words and their count as a map where key is word (from the file)
    // and value is number (count of the word)
    map: Map<string, number>;
    // Error message, mainly for testing purpose
    errorMessage: string;

    // Constructor
    constructor(){
        this.inputFile = '';
        this.map = new Map<string, number>();
        this.errorMessage = '';
    }

    // Error message getter
    public getErrorMessage(): string{
        return this.errorMessage;
    } 

    // Map getter
    public getMap(): Map<string, number>{
        return this.map;
    }

    // Public method to execute counting
    public execute(inputFile: string){
        // Update input file
        this.inputFile = inputFile;
        // Check if: input is given; input is with '.txt' extension; input file exists
        this._checkInput();
        // If file was found, lets count words! 
        if (!this.errorMessage) this.map = this._countWords();
    }

    // Function to check input
    private _checkInput(): void{
        // If nothing was found as a third argument (input file), we let user know
        // Also, store the error message
        if (!this.inputFile) this._noInputFile();

        // If entered filename was not with '.txt' extension, we let user know and 
        // try to re-run with correct extension.
        // Also, store the error message
        else if (this.inputFile.substring(this.inputFile.length - 4) != '.txt') this._wrongExtension();

        // If user entered a correct filename but it doesn't exist, we let user know
        // Also, store the error message
        else if (!fs.existsSync(this.inputFile)) this._fileNotFound();
    }

    // Use underscore ('_') to see easily which function is private
    // Yes, TypeScript does some work for us here and this method might be a little bit of oldschool,
    // but for me, it is more clear and I can instantly see which function is private and which is not.
    //
    // Function to log that no input file were given
    private _noInputFile(): void {
        // Log error message 
        console.log("\nPlease enter filename!");
        // Update error message
        this.errorMessage = 'no input file';
    }

    // Function to re-run program with '.txt' extension
    private _wrongExtension(): void {
        // Let user know that no '.txt' extension was used
        console.log("\nEntered file was not with '.txt' extension.");

        // Add '.txt' extension
        this.inputFile += '.txt';

        // Log that program will try to re-run with given input, but with '.txt' extension
        console.log(`\nTrying to open '${this.inputFile}'...`);

        // Re-run and map the results 
        this.execute(this.inputFile);
        
        // Log results
        this.logResults(this.map);

        // Clear map because we already re-ran the count function and logged results
        this.clearMap();
        
        // Update error message
        this.errorMessage = 'wrong extension';
    }

    // Function to let user know that file was not found
    private _fileNotFound(): void {
        // Log error message
        console.log("\nFile was not found!");
        // Update error message
        this.errorMessage = 'file not found';
    }

    // Function to count words
    private _countWords(): Map<string, number> {
        // Read existing given file
        let file: string = fs.readFileSync(this.inputFile,'utf8');
            
        // All words from given file
        // Trim all unnecessary spaces, split words, remove whitespaces and finally create a new array
        let words: string[] = file.trim().split(/\s+/).map(function(word: string) {
            // Turn words to lowercase so hello, hEllo, heLLO, Hello etc would be the same
            // If word last character is any of these => , . ; : ! ? ) ' " 
            // Or first character is any of these => ( ' "
            // remove it by replacing it with ''.
            return word.replace(/\,$|\.$|\:$|\;$|\?$|\!$|\)$|\"$|\'$|^\(|^\"|^\'/g, '').toLowerCase();
        });
            
        // Sort words alphabetically
        words.sort();

        // Loop through all words and start mapping
        for (let word of words){
            // If map includes given word we increase its value by 1
            // If not, then we set the word as a key and 1 as a value
            // PS! I'm using non-null assertion operator ("!") because I'm sure
            // map.get(word) has value, meaning it is not undefined.
            this.map.has(word) ? this.map.set(word, this.map.get(word)! + 1) : this.map.set(word, 1);
            }
        
        // Return map. Keys are words, values are counts.
        return this.map;
    }

    // Method to clear the map
    public clearMap(){
        this.map.clear();
    }

    // Log results
    public logResults(result: Map<string, number>): void{
        // Log an empty line for cleaner terminal
        console.log('');
        // Loop through the map
        result.forEach((value: number, key: string) => {
            // Cleaning words with regular expressions can cause empty words
            // Example: 'Hello, world !' - words would be mapped like this: 
            // hello: 1
            // world: 1
            //  : 1
            // That's because we remove marks/signs from word endings and solo sign
            // will be removed and the result would be empty string ('')
            if (key != '') console.log(`${key}: ${value}`);

            // Else if key == '' and map size is 1, means file does not contain any words
            // Let user know if file does not contain any words
            else if (result.size == 1) console.log(`'${this.inputFile}' does not contain any words!`);
        });
    }
}