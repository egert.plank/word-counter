// To ensure we use strict mode 
'use strict'
// Import Counter class
import {Counter} from './counter';

// Our input file will be 3rd argument from command line
let inputFile = process.argv[2];
// Create a counter
let counter = new Counter();
// Start counting
counter.execute(inputFile)
// Log results
counter.logResults(counter.getMap());