## Name
Word Counter



## Description
Adaptavist Junior Software Engineer
Task 1 - JavaScript/TypeScript Word Count Challenge

Word Counter is a simple application that reads a text content from a file, counts each unique word, and prints the output into the console.
Some very basic tests are added to the project. 

Technologies used: Node.js, TypeScript, Jest.



## Instructions to Run and Build
Please make sure you are located in the project folder.
To run the app, run 
`ts-node word-counter.ts /path/to/file`
in your terminal.
For example: "ts-node word-counter.js ./test/input5.txt".

To run tests, run: 
`npm test`.


## Explanation
I could have done this assessment using functions only, but I wanted it to be a little bit more complex.
I created a class Counter, that has several functions inside. Public functions are execute() and 
logResults(), other functions are accessible only inside the class.

I am pretty detail oriented, meaning I had to do input check. The program lets user know when there's a issue with input file (no file given, wrong extension or no file found). Program re-runs when given file extension was not '.txt' by adding right extension to the input.

I decided to clean words from marks and signs by removing => ( ' "  from the beginning of each word
and removing => , . ; : ! ? " ' ) from the end of each word. I hope this is enough, but could have gone deeper with RegEx.

There are total of nine tests, which are:
- Whitespaces are removed correctly 
- Marks/signs are removed correctly
- Count the same words regardless of the case sensitivity
- Testing file without words 
- Testing error message: No input file!
- Testing error message: Wrong or no extension! 
- Testing error message: File was not found!
- Some more basic tests with a textfile
- And some more tests with a textfile...

The purpose of this assessment is to show that I'm able to write modular and reusable code. As told, I could have done this assignment with around 50-70 lines of code and the program would look very simple, but this is not the best practise.

PS! I do have a more simple version of this assessment. I can present it if simplicity is the key, not modularity.




 
