import {Counter} from './counter';

// To ignore console logs while testing
jest.spyOn(console, 'log').mockImplementation(jest.fn());

// Exptected count, real count
let expectedCount: number;
// Actual count from our program
let actualCount: number;

// Expected error, real error
let expectedError: string;
// Actual error from our program
let actualError: string;

// Counter
let counter = new Counter();

// Testing whitespaces 
test('Whitespaces are removed correctly', () => {
    // Execute counter
    counter.execute('./test/input1.txt');
    // Our result: how many different words our map include?
    actualCount = counter.getMap().size;
    // Expected/real amount
    expectedCount = 22;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

// Testing marks and signs
test("Marks/signs are removed correctly", () => {
    // Execute counter
    counter.execute('./test/input2.txt');
    // Our result: how many different words our map include?
    actualCount = counter.getMap().size;
    // Expected/real amount
    expectedCount = 4;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

//Testing case sensitivity
test('Count the same words regardless of the case sensitivity', () => {
    // Execute counter
    counter.execute('./test/input3.txt');
    // Our result: how many different words our map include?
    let map = counter.getMap();
    // We expect that we have 1 word as a key
    actualCount = map.size;
    expectedCount = 1;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // And we expect to have 12 as a value. Test file contains
    // 12 * 'adaptavist', but every word is written differently
    actualCount = map.get('adaptavist')!;
    expectedCount = 12;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

// Testing file without words
test('Testing file without words', () => {
    // Execute counter
    counter.execute('./test/input6.txt');
    // Count how much empty string we get
    // PS! Empty strings are not logged/displayed when running the program
    let emptyWord = counter.getMap().get('');
    expectedCount = 6;
    // Compare results
    expect(emptyWord).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

// Testing some text
test('Some more basic tests with a textfile', () => {
    // Execute counter
    counter.execute('./test/input4.txt');
    // Our result: how many different words our map include?
    actualCount = counter.getMap().size;
    expectedCount = 66;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

// Another text testing
test('And some more tests with a textfile...', () => {
    // Execute counter
    counter.execute('./test/input5.txt');
    // Our result: how many different words our map include?
    actualCount = counter.getMap().size;
    expectedCount = 85;
    // Compare results
    expect(actualCount).toBe(expectedCount);
    // Clear the result
    counter.clearMap();
});

// Test error message: No input file
test('Testing error message: No input file!', () => {
    // Execute counter
    counter.execute('');
    // Get error message from our program
    actualError = counter.getErrorMessage();
    // Expected, real error that should be thrown
    expectedError = 'no input file';
    // Compare results
    expect(actualError).toBe(expectedError);
});

// Test error message: Wrong extension
test('Testing error message: Wrong or no extension!', () => {
    // Execute counter
    counter.execute('./test/input5');
    // Get error message from our program
    actualError = counter.getErrorMessage();
    // Expected, real error that should be thrown
    expectedError = 'wrong extension';
    // Compare results
    expect(actualError).toBe(expectedError);
});

// Test error message: File was not found
test('Testing error message: File was not found!', () => {
    // Execute counter
    counter.execute('./test/wrongFileNameHere.txt');
    // Get error message from our program
    actualError = counter.getErrorMessage();
    // Expected, real error that should be thrown
    expectedError = 'file not found';
    // Compare results
    expect(actualError).toBe(expectedError);
});